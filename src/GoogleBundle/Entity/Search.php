<?php

namespace GoogleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Search
 *
 * @ORM\Table(name="search",uniqueConstraints={@ORM\UniqueConstraint(name="search_idx",  columns={"date", "device_id", "query_id"})})
 * @ORM\Entity(repositoryClass="GoogleBundle\Repository\SearchRepository")
 */
class Search
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="clicks", type="integer", nullable=true)
     */
    private $clicks;

    /**
     * @var int
     *
     * @ORM\Column(name="impressions", type="integer", nullable=true)
     */
    private $impressions;

    /**
     * @var float
     *
     * @ORM\Column(name="ctr", type="float", nullable=true)
     */
    private $ctr;

    /**
     * @var float
     *
     * @ORM\Column(name="position", type="float", nullable=true)
     */
    private $position;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
    * @var int
    *
    * @ORM\ManyToOne(targetEntity="Query", inversedBy="search", cascade={"persist"})
    * @ORM\JoinColumn(name="query_id", referencedColumnName="id")
    */
    private $query;

    /**
    * @var int
    *
    * @ORM\ManyToOne(targetEntity="Device", inversedBy="search")
    * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
    */
    private $device;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clicks
     *
     * @param integer $clicks
     *
     * @return Search
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;

        return $this;
    }

    /**
     * Get clicks
     *
     * @return int
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set impressions
     *
     * @param integer $impressions
     *
     * @return Search
     */
    public function setImpressions($impressions)
    {
        $this->impressions = $impressions;

        return $this;
    }

    /**
     * Get impressions
     *
     * @return int
     */
    public function getImpressions()
    {
        return $this->impressions;
    }

    /**
     * Set ctr
     *
     * @param float $ctr
     *
     * @return Search
     */
    public function setCtr($ctr)
    {
        $this->ctr = $ctr;

        return $this;
    }

    /**
     * Get ctr
     *
     * @return float
     */
    public function getCtr()
    {
        return $this->ctr;
    }

    /**
     * Set position
     *
     * @param float $position
     *
     * @return Search
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return float
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Search
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set device
     *
     * @param \GoogleBundle\Entity\Device $device
     *
     * @return Device
     */
    public function setDevice(Device $device = null)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * Get device
     *
     * @return \GoogleBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

      /**
     * Set query
     *
     * @param \GoogleBundle\Entity\Device $query
     *
     * @return Device
     */
    public function setQuery(Query $query = null)
    {
        $this->query = $query;
        return $this;
    }

    /**
     * Get query
     *
     * @return \GoogleBundle\Entity\Device
     */
    public function getQuery()
    {
        return $this->query;
    }
}

