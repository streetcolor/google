<?php
namespace GoogleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\HttpFoundation\JsonResponse;


use GoogleBundle\Document\Query;
use GoogleBundle\Document\Device;

use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{


    public function connectAction()
    {	
    	$oauth = false;
    	//Oauth2
    	if($oauth==true){
	    	$session = new Session();
	    	$authUrl  = "";
			
			if (!$oauth_credentials = $this->getOAuthCredentialsFile()) {
				echo $this->missingOAuth2CredentialsWarning();
				exit;
			}

			$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

			$client = new \Google_Client();
			$client->setAuthConfig($oauth_credentials);
			$client->setRedirectUri($redirect_uri);
			$client->setScopes('https://www.googleapis.com/auth/webmasters.readonly');

			if (! isset($_GET['code'])) {
			  $auth_url = $client->createAuthUrl();
			} 
			else {

				$client->authenticate($_GET['code']);

				$session->set('access_token', $client->getAccessToken());
				return new RedirectResponse($this->generateUrl('google_home'));
			}

			 return $this->render('GoogleBundle:Default:index.html.twig', array('authUrl' => $auth_url));

		}

		return new RedirectResponse($this->generateUrl('google_home'));
       
    }

	public function homeAction(Request $request, $_locale, $support, $range	)
	{	
 
    	$campaign = null;
    	if(isset($_GET['campaign'])){
    		$campaign = $_GET['campaign'];
		}
    	$searchanalytics = null;
    	if($this->get('google.check.internet.service')->CheckInternet()){
	    	$session = new Session();
			

			$GoogleCient = $this->get('google.token.authenticated');
			$searchanalytics = $this->get('google.gwt.service')->LoadSearchConsole();
						
		}
		else{

		}
		$stats = $this->generateStatistics($searchanalytics, $_locale, $support, $campaign);

		//var_dump($this->generateQueries($searchanalytics, $_locale, $support, $campaign, "2015-12-19", "2015-12-19"));

		$cal_day = [];
		$weeks   = [];
		$months  = [];

		foreach ($stats as $key => $stat) {
 				$date                       = new \DateTime($stat["date"]);

				$cal_day[$key]["date"]                              = $stat["date"];
				$cal_day[$key]["clicks"]                            = $stat[$support."_clicks"];
				$cal_day[$key]["ctr"]                               = round($stat[$support."_ctr"],4);
				$cal_day[$key]["impressions"]                       = $stat[$support."_impressions"];
				$cal_day[$key]["position"]                          = round($stat[$support."_position"],2);

				$weeks[$date->format("W")][$key]["date"]            = $stat["date"];
				$weeks[$date->format("W")][$key]["clicks"]          = $stat[$support."_clicks"];
				$weeks[$date->format("W")][$key]["ctr"]             = round($stat[$support."_ctr"],4);
				$weeks[$date->format("W")][$key]["impressions"]     = $stat[$support."_impressions"];
				$weeks[$date->format("W")][$key]["position"]        = round($stat[$support."_position"],2);

				$months[$date->format("m/Y")][$key]["date"]         = $stat["date"];
				$months[$date->format("m/Y")][$key]["clicks"]       = $stat[$support."_clicks"];
				$months[$date->format("m/Y")][$key]["ctr"]          = round($stat[$support."_ctr"],4);
				$months[$date->format("m/Y")][$key]["impressions"]  = $stat[$support."_impressions"];
				$months[$date->format("m/Y")][$key]["position"]     = round($stat[$support."_position"],2);
				
				$this->generateQueries($searchanalytics, $_locale, $support, $campaign, $stat["date"], $stat["date"]);


		}
		$cal_month = [];
		foreach ($months as $key_month => $days) {
			$click       = 0;
			$ctr         = 0;
			$impressions = 0;
			$position    = 0;
			$i           = 0;
			
			foreach ($days as $key => $day) {
				$i++;
				$click       += $day['clicks'];
				$ctr         += $day['ctr'];
				$impressions += $day['impressions'];
				$position    += $day['position'];
			}
			$cal_month[$key_month]['date']        = $key_month;
			$cal_month[$key_month]['clicks']      = $click;
			$cal_month[$key_month]['ctr']         = $ctr/$i;
			$cal_month[$key_month]['impressions'] = $impressions;
			$cal_month[$key_month]['position']    = $position/$i;
		}

		$cal_week  = [];
		foreach ($weeks as $key_week => $days) {
			$click       = 0;
			$ctr         = 0;
			$impressions = 0;
			$position    = 0;
			$i           = 0;


			foreach ($days as $key => $day) {
				$i++;
				$click       += $day['clicks'];
				$ctr         += $day['ctr'];
				$impressions += $day['impressions'];
				$position    += $day['position'];
			}
			$start =  array_values($days)[0]['date'];
			$end   = end($days)['date'];

			$cal_week[$key_week]['date']        = $start. " - ".$end;
			$cal_week[$key_week]['clicks']      = $click;
			$cal_week[$key_week]['ctr']         = $ctr/$i;
			$cal_week[$key_week]['impressions'] = $impressions;
			$cal_week[$key_week]['position']    = $position/$i;
		}

        if($request->isXmlHttpRequest()) {

			$response = new JsonResponse();
			return $response->setData(array(
				'status'  =>'success',
			    'content' => ${'cal_'.$range}
			));


		}
		else{


// $request = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;					
// $repositoryDevice = $manager->getRepository('GoogleBundle:Device');

// $device = $repositoryDevice->findById($keyDevice)[0];

// $clicks       = 1 ; //$stat['clicks']?: 'null';
// $impressions  = 1 ; //$stat['impressions']?: 'null';
// $ctr          = 1 ; //$stat['ctr']?: 'null';
// $position     = 1 ; //$stat['position']?: 'null';

// $trafic = new Trafic();
// $trafic->setClicks($clicks);
// $trafic->setImpressions($impressions);
// $trafic->setCtr($ctr);
// $trafic->setPosition($position);
// $trafic->setDate($stat['keys'][0]);
// $trafic->setDevice($device);

// $manager->persist($trafic);
// $manager->flush();



	        return $this->render('GoogleBundle:Default:home.html.twig', array(
																        	'stats'=>${'cal_'.$range}, 
																        	'locale'=>$_locale, 
																        	'support'=>$support)
																        );
		}

    }

    public function generateStatistics($searchanalytics="", $_locale="fr", $support='all', $campaign=""){

		$dm = $this->get('doctrine_mongodb')->getManager();

		$device = new Device();
		$device->setName('All');
		$dm->persist($device);

		$device = new Device();
		$device->setName('Desktop');
		$dm->persist($device);

		$device = new Device();
		$device->setName('Mobile');
		$dm->persist($device);

		$device = new Device();
		$device->setName('Tablet');
		$dm->persist($device);



    	$stats               = [];
    	$return              = [];
    	$stats['2015-09-01'] = array(
									"date"        => '2015-09-01',
									"all_clicks"          => 0,
									"all_ctr"             => 0,
									"all_impressions"     => 0,
									"all_position"        => 0,
									"desktop_clicks"      => 0,
									"desktop_ctr"         => 0,
									"desktop_impressions" => 0,
									"desktop_position"    => 0,
									"mobile_clicks"       => 0,
									"mobile_ctr"          => 0,
									"mobile_impressions"  => 0,
									"mobile_position"     => 0,
									"tablet_clicks"       => 0,
									"tablet_ctr"          => 0,
									"tablet_impressions"  => 0,
									"tablet_position"     => 0
							);


    	$handle = realpath('.').'/csv/'.$_locale.'_stats'.$campaign.'.csv';  
		$fileRewrite = fopen($handle, "a+");

		//Get Existing entries from CSV/Model
		
		while($stat = fgetcsv($fileRewrite,1024,';')){

			$stats[$stat[0]] =array(
								"date"                => $stat[0],
								"all_clicks"          => $stat[1],
								"all_ctr"             => $stat[2],
								"all_impressions"     => $stat[3],
								"all_position"        => $stat[4],
								"desktop_clicks"      => $stat[5],
								"desktop_ctr"         => $stat[6],
								"desktop_impressions" => $stat[7],
								"desktop_position"    => $stat[8],
								"mobile_clicks"       => $stat[9],
								"mobile_ctr"          => $stat[10],
								"mobile_impressions"  => $stat[11],
								"mobile_position"     => $stat[12],
								"tablet_clicks"       => $stat[13],
								"tablet_ctr"          => $stat[14],
								"tablet_impressions"  => $stat[15],
								"tablet_position"     => $stat[16],
							);
		}

    	if($this->get('google.check.internet.service')->CheckInternet()){
			
			$devices = array('all', "desktop", "mobile", "tablet");

			$endDate   = end($stats);

			$startDate = $endDate["date"] ?: "2015-09-01";

			$endDate = (date('Y-m-d'));

			foreach ($devices as $device) {
				
				$request = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;					

				$request->setStartDate($startDate);
				$request->setEndDate($endDate);
				$request->setDimensions(['date']);

				$filter  = array();
				$filter2 = array();
				if($device != "all"){

					$filter = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter->setDimension('device');
					$filter->setOperator('equals');
					$filter->setExpression(strtoupper($device));
				}

				if($campaign != null){
					$filter2 = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter2->setDimension("query");
					$filter2->setOperator("contains");
					$filter2->setExpression($campaign);
				}

				$filtergroup = new \Google_Service_Webmasters_ApiDimensionFilterGroup();
				$filtergroup->setFilters(array($filter, $filter2));
				$request->setDimensionFilterGroups(array($filtergroup));

				$qsearch = $searchanalytics->query("http://www.streetco.com/$_locale", $request); 
				$rows = $qsearch->getRows();

				foreach ($rows as $key => $stat) {
					
					if(!array_key_exists($stat['keys'][0], $stats)){
						$return[$stat['keys'][0]]['date']                 = $stat['keys'][0];
						$return[$stat['keys'][0]][$device."_clicks"]      = $stat['clicks'];
						$return[$stat['keys'][0]][$device."_ctr"]         = $stat['ctr'];
						$return[$stat['keys'][0]][$device."_impressions"] = $stat['impressions'];
						$return[$stat['keys'][0]][$device."_position"]    = $stat['position'];

						// $query = new Query();
						// $query->setClicks($stat['clicks']);
						// $query->setImpressions($stat['impressions']);
						// $query->setCtr($stat['ctr']);
						// $query->setPosition($stat['position']);
						// $query->setDate($stat['keys'][0]);

						// $dm = $this->get('doctrine_mongodb')->getManager();
						// $dm->persist($query);
						// $dm->flush();

					}

				}

			
			}

			foreach ($return as $key => $item) {

				$stats[$key] = $item;

				fputcsv($fileRewrite, $item, ';');

			}
			
			fclose($fileRewrite);
		}
	
		return $stats;
    }


 	public function generateQueries($searchanalytics="", $_locale="fr", $support='all', $campaign="", $start="", $end=""){

		$handle = realpath('.').'/csv/'.$_locale.'_queries_'.$start.'_'.$campaign.'.csv';
		  
 		if(!file_exists($handle)){


			$devices = array('all', "desktop", "mobile", "tablet");


			$return  = [];

			foreach ($devices as $device) {
				
				$request = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;	
				$request->setStartDate($start);
				$request->setEndDate($end);
				$request->setDimensions(['query']);
				//$request->setRowLimit(10);

				$filter  = array();
				$filter2 = array();

				if($device != "all"){

					$filter = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter->setDimension('device');
					$filter->setOperator('equals');
					$filter->setExpression(strtoupper($device));
				}

				if($campaign != null){
					$filter2 = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter2->setDimension("query");
					$filter2->setOperator("contains");
					$filter2->setExpression($campaign);
				}

				$filtergroup = new \Google_Service_Webmasters_ApiDimensionFilterGroup();
				$filtergroup->setFilters(array($filter, $filter2));
				$request->setDimensionFilterGroups(array($filtergroup));

				$qsearch = $searchanalytics->query("http://www.streetco.com/$_locale", $request); 
				$rows = $qsearch->getRows();

				foreach ($rows as $key => $stat) {

						$keyword = implode(',', $stat['keys']);

						$return[$keyword][$keyword]  = $keyword;
						$return[$keyword][$device."_clicks"]      = $stat['clicks'];
						$return[$keyword][$device."_ctr"]         = $stat['ctr'];
						$return[$keyword][$device."_impressions"] = $stat['impressions'];
						$return[$keyword][$device."_position"]    = $stat['position'];


				}



			}
			
			
			$fileRewrite = fopen($handle, "a+");
			
			foreach ($return as $key => $item) {

				fputcsv($fileRewrite, $item, ';');

			}
			
			fclose($fileRewrite);
			
			return $return;
		}
    }

    
	
}
