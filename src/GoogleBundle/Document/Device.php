<?PHP
namespace GoogleBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Column;

/** @ODM\Document(db="GWT", collection="gwt_device") */
class Device
{

    /**
    * @ODM\Id
    * @Column(type="integer")
    * @GeneratedValue(strategy="AUTO")
    */
    protected $id; 

    /** @ODM\String @ODM\Index(unique=true, order="asc") */
	protected $name;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
}
