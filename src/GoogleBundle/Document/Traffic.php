<?PHP
namespace GoogleBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Column;

/** @ODM\Document(db="GWT", collection="gwt_traffic") 
* @ODM\UniqueIndex(keys={"date"="asc", "device"="asc", "device"="campaign"})
*/
class Traffic
{

    /**
    * @ODM\Id
    * @Column(type="integer")
    * @GeneratedValue(strategy="AUTO")
    */
    protected $id; 

    /** @ODM\Int */
	protected $clicks;

    /** @ODM\Int */
    protected $impressions;

    /** @ODM\String */
    protected $ctr;

    /** @ODM\String */
    protected $position;

    /** @ODM\Date */
    protected $date;

    /** @ODM\ReferenceOne(targetDocument="Device") */
    protected $device;

    /** @ODM\ReferenceOne(targetDocument="Campaign") */
    protected $campaign;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clicks
     *
     * @param int $clicks
     * @return self
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
        return $this;
    }

    /**
     * Get clicks
     *
     * @return int $clicks
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set impressions
     *
     * @param int $impressions
     * @return self
     */
    public function setImpressions($impressions)
    {
        $this->impressions = $impressions;
        return $this;
    }

    /**
     * Get impressions
     *
     * @return int $impressions
     */
    public function getImpressions()
    {
        return $this->impressions;
    }

    /**
     * Set ctr
     *
     * @param string $ctr
     * @return self
     */
    public function setCtr($ctr)
    {
        $this->ctr = $ctr;
        return $this;
    }

    /**
     * Get ctr
     *
     * @return string $ctr
     */
    public function getCtr()
    {
        return $this->ctr;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return date $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set device
     *
     * @param GoogleBundle\Document\Device $device
     * @return self
     */
    public function setDevice(\GoogleBundle\Document\Device $device)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * Get device
     *
     * @return GoogleBundle\Document\Device $device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set campaign
     *
     * @param GoogleBundle\Document\Campaign $campaign
     * @return self
     */
    public function setCampaign(\GoogleBundle\Document\Campaign $campaign)
    {
        $this->campaign = $campaign;
        return $this;
    }

    /**
     * Get campaign
     *
     * @return GoogleBundle\Document\Campaign $campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}
