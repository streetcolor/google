<?php
namespace GoogleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use GoogleBundle\Entity\Trafic;
use GoogleBundle\Entity\Query;
use GoogleBundle\Entity\Device;
use GoogleBundle\Entity\Campaign;
use GoogleBundle\Entity\Search;

use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{

	public function homeAction(Request $request, $_locale, $support, $range	)
	{	

    	$campaign = null;
    	$manager            = $this->get('doctrine')->getManager();

    	if(isset($_GET['campaign'])){
    		$campaign = $_GET['campaign'];
		}
    	$searchanalytics = null;
    	if($this->get('google.check.internet.service')->CheckInternet()){
	    
			$GoogleCient = $this->get('google.token.authenticated');
			$searchanalytics = $this->get('google.gwt.service')->LoadSearchConsole();
						
		}

		$stats = $this->generateStatistics($manager, $searchanalytics, $_locale, $campaign);
		
		$repositoryDevice   = $manager->getRepository('GoogleBundle:Device');
		$repositoryTrafic   = $manager->getRepository('GoogleBundle:Trafic');
		$repositoryCampaign = $manager->getRepository('GoogleBundle:Campaign');

		$singleDevice       = $repositoryDevice->findOneByName($support);

		$singleCampaign     = $repositoryCampaign->findOneById(1);


		if($range=="month"){
			
			$listDates          = $repositoryTrafic->findByMonth($singleDevice->getId());
		
		}
		elseif($range=="week"){
			
			$listDates          = $repositoryTrafic->findByWeek($singleDevice->getId());
		
		}
		else{
			
			$listDates          = $repositoryTrafic->findBy(
														  array(
														  		'device'   => $singleDevice,
														  		'campaign' => $singleCampaign
														  		), // Critere
														  array(
														  		'date' => 'ASC'
														  		)
														);
		}
		
		$data['start'] ='2015-10-10';
		$data['end'] ='2015-10-10';
		$this->generateQueries($manager, $searchanalytics,$_locale, $data );

		// foreach ($listDates as $key => $value) {
		// 	$data['start'] = $value->getDate();
		// 	$data['end'] = $value->getDate();
		// 	$this->generateQueries($manager, $searchanalytics,$_locale, $data );
		// }

        if($request->isXmlHttpRequest()) {
			$encoders = array(new XmlEncoder(), new JsonEncoder());
			$normalizers = array(new ObjectNormalizer());

			$return = array(
					'status'  =>'success',
				    'content' => $listDates
				);
			$serializer = new Serializer($normalizers, $encoders);
        	$jsonContent = $serializer->serialize($return, 'json');

			  return new Response($jsonContent);

		}
		else{

	        return $this->render('GoogleBundle:Default:home.html.twig', array(
																        	'stats'=>$listDates, 
																        	'locale'=>$_locale, 
																        	'support'=>$support)
																        );
		}

    }

    public function generateStatistics(\Doctrine\ORM\EntityManager $manager, \Google_Service_Webmasters_Searchanalytics_Resource $searchanalytics, $_locale="fr", $campaign=""){

    	if($this->get('google.check.internet.service')->CheckInternet()){
			
			$devices = array("all", "desktop", "mobile", "tablet");
			
			$repositoryDevice   = $manager->getRepository('GoogleBundle:Device');
			$repositoryTrafic   = $manager->getRepository('GoogleBundle:Trafic');
			$repositoryCampaign = $manager->getRepository('GoogleBundle:Campaign');

			$allDevices         = $repositoryDevice->findAll();
			$singleCampaign     = $repositoryCampaign->findOneById(1);
 	
			$lastOne            = $repositoryTrafic->findLastEntry();

			if(count($lastOne)){
				$lastOne = $lastOne[0]->getDate(false);
				$lastOne->add(new \DateInterval('P1D'));
				$startDate = $lastOne->format('Y-m-d');
			}
			else{
				$startDate = '2015-09-01';
			}

			$endDate   = date('Y-m-d');
			$datas = [];

			foreach ($devices as $keyDevice => $device) {
				
				$request = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;					

				$request->setStartDate($startDate);
				$request->setEndDate($endDate);
				$request->setDimensions(['date']);

				$filter  = array();
				$filter2 = array();
				if($device != "all"){

					$filter = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter->setDimension('device');
					$filter->setOperator('equals');
					$filter->setExpression(strtoupper($device));
				}

				if($campaign != null){
					$filter2 = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter2->setDimension("query");
					$filter2->setOperator("contains");
					$filter2->setExpression($campaign);
				}

				$filtergroup = new \Google_Service_Webmasters_ApiDimensionFilterGroup();
				$filtergroup->setFilters(array($filter, $filter2));
				$request->setDimensionFilterGroups(array($filtergroup));

				$qsearch = $searchanalytics->query("http://www.streetco.com/$_locale", $request); 
				$rows = $qsearch->getRows();

				foreach ($rows as $key => $stat) {

					$datas[$stat['keys'][0]][$keyDevice] =  [
																'date'        => $stat['keys'][0],
																'clicks'      => $stat['clicks'],
																'impressions' => $stat['impressions'],
																'ctr'         => $stat['ctr'],
																'position'    => $stat['position']
															];
					
				}
			
			}

			foreach ($datas as $date => $stats) {
				
				foreach ($stats as $id_device => $stat) {

					$trafic = new Trafic();
					$trafic->setClicks($stat['clicks']);
					$trafic->setImpressions($stat['impressions']);
					$trafic->setCtr($stat['ctr']);
					$trafic->setPosition($stat['position']);
					$trafic->setDate($stat['date']);

					$trafic->setDevice($allDevices[$id_device]);
					$trafic->setCampaign($singleCampaign);

					$manager->persist($trafic);
				}
				
			}
		}

		$manager->flush();

		return true;
    }


 	public function generateQueries(\Doctrine\ORM\EntityManager $manager, \Google_Service_Webmasters_Searchanalytics_Resource $searchanalytics, $_locale="fr", $data=array()){

 			$campaign = null;
			
			$repositoryQuery    = $manager->getRepository('GoogleBundle:Query');
			$repositoryDevice   = $manager->getRepository('GoogleBundle:Device');
			$repositoryTrafic   = $manager->getRepository('GoogleBundle:Trafic');
			$repositorySearch   = $manager->getRepository('GoogleBundle:Search');

			$allDevices         = $repositoryDevice->findAll();
			$getAllQueries      = $repositoryQuery->findAll();

			$queries = [];
			foreach ($getAllQueries as $key => $value) {
				$queries[$value->getName()] = $value;
			}


			$return  = [];

			foreach ($allDevices as $device) {
				
				$request = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;	
				$request->setStartDate($data['start']);
				$request->setEndDate($data['end']);
				$request->setDimensions(['query']);
				//$request->setRowLimit(10);

				$filter  = array();
				$filter2 = array();

				if($device->getName() != "All"){

					$filter = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter->setDimension('device');
					$filter->setOperator('equals');
					$filter->setExpression(strtoupper($device->getName()));
				}

				if($campaign != null){
					$filter2 = new \Google_Service_Webmasters_ApiDimensionFilter();
					$filter2->setDimension("query");
					$filter2->setOperator("contains");
					$filter2->setExpression($campaign);
				}

				$filtergroup = new \Google_Service_Webmasters_ApiDimensionFilterGroup();
				$filtergroup->setFilters(array($filter, $filter2));
				$request->setDimensionFilterGroups(array($filtergroup));

				$qsearch = $searchanalytics->query("http://www.streetco.com/$_locale", $request); 
				$rows = $qsearch->getRows();

				foreach ($rows as $key => $stat) {

					$keyword = substr(htmlentities(implode(',', $stat['keys'])), 0, 254);
				
					if(!array_key_exists($keyword, $queries)){

						$queries[$keyword] = $keyword;
						$query  = new Query();
						$query->setName($keyword);
			
					}
					else{
						$query = $queries[$keyword];
					}

					$search = new Search();
					$search->setQuery($query);
					$search->setDevice($device);
					$search->setClicks($stat['clicks']);
					$search->setImpressions($stat['impressions']);
					$search->setCtr($stat['ctr']);
					$search->setPosition($stat['position']);
					//$search->setDate($data['start']);
					$manager->persist($search);


					
				}			

			}
			$manager->flush();
			return $return;
		
    }

    
	
}
