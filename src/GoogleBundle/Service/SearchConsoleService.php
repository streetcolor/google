<?php
namespace GoogleBundle\Service;


class SearchConsoleService 
{

	private $GoogleCient;

	public function __construct(\Google_client $GoogleCient)
	{
		$this->GoogleCient    = $GoogleCient;
	}

	public function LoadSearchConsole()
	{
		$this->GoogleCient->setScopes('https://www.googleapis.com/auth/webmasters.readonly');
		$webmastersService = new \Google_Service_Webmasters($this->GoogleCient);
		$searchanalytics = $webmastersService->searchanalytics;

		return $searchanalytics;
	}	
}