<?php

namespace GoogleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trafic
 *
 * @ORM\Table(name="trafic",uniqueConstraints={@ORM\UniqueConstraint(name="trafic_idx", columns={"date", "device_id", "campaign_id"})})
 * @ORM\Entity(repositoryClass="GoogleBundle\Repository\TraficRepository")
 */


class Trafic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="clicks", type="integer", nullable=true)
     */
    private $clicks;

    /**
     * @var int
     *
     * @ORM\Column(name="impressions", type="integer", nullable=true)
     */
    private $impressions;

    /**
     * @var float
     *
     * @ORM\Column(name="ctr", type="float", nullable=true)
     */
    private $ctr;

    /**
     * @var float
     *
     * @ORM\Column(name="position", type="float", nullable=true)
     */
    private $position;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;
    
    /**
    * @var int
    *
    * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="trafic")
    * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
    */
    private $campaign;

    /**
    * @var int
    *
    * @ORM\ManyToOne(targetEntity="Device", inversedBy="trafic")
    * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
    */
    private $device;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clicks
     *
     * @param integer $clicks
     *
     * @return Trafic
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;

        return $this;
    }

    /**
     * Get clicks
     *
     * @return int
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set impressions
     *
     * @param integer $impressions
     *
     * @return Trafic
     */
    public function setImpressions($impressions)
    {
        $this->impressions = $impressions;

        return $this;
    }

    /**
     * Get impressions
     *
     * @return int
     */
    public function getImpressions()
    {
        return $this->impressions;
    }

    /**
     * Set ctr
     *
     * @param float $ctr
     *
     * @return Trafic
     */
    public function setCtr($ctr)
    {
        $this->ctr = $ctr;

        return $this;
    }

    /**
     * Get ctr
     *
     * @return float
     */
    public function getCtr()
    {
        return round($this->ctr*100, 2)." %";
    }

    /**
     * Set position
     *
     * @param float $position
     *
     * @return Trafic
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return float
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Trafic
     */
    public function setDate($date)
    {   
        $this->date = new \DateTime($date);

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate($format=true)
    {   
        return $format ?  $this->date->format('Y-m-d') : $this->date;
    }

     /**
     * Set device
     *
     * @param \GoogleBundle\Entity\Device $device
     *
     * @return Device
     */
    public function setDevice(Device $device = null)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * Get device
     *
     * @return \GoogleBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

      /**
     * Set campaign
     *
     * @param \GoogleBundle\Entity\Device $campaign
     *
     * @return Device
     */
    public function setCampaign(Campaign $campaign = null)
    {
        $this->campaign = $campaign;
        return $this;
    }

    /**
     * Get campaign
     *
     * @return \GoogleBundle\Entity\Device
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}

