<?php
namespace GoogleBundle\Service;

use Symfony\Component\HttpFoundation\Response;

class TokenAuthenticatedFactory
{

  //Static => singleton used withing from service factory
  public static function MakeAuthentification()
  {

    $application_creds = __DIR__ . '/../../../A-2c47ff0ada73.json';
    $credentials_file =  file_exists($application_creds) ? $application_creds : false;

    $GoogleClient = new \Google_Client();

    if ($credentials_file) {
      // set the location manually
      $GoogleClient->setAuthConfig($credentials_file);
    } elseif (getenv(__DIR__ . '/../../../A-2c47ff0ada73.json')) {
      // use the application default credentials
      $GoogleClient->useApplicationDefaultCredentials();
    } else {
      echo missingServiceAccountDetailsWarning();
      exit;
    } 

    return $GoogleClient;
  }


}